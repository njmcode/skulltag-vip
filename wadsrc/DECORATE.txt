// Custom class for the Tom player:
//	# slower than normal
//	# has a custom weapon
//	# can not pick any regular items up
//	- has a set colour
//	# leaves a trail
//	# grants a point to the assassin team on death
actor TomPlayer : DoomPlayer {

	Speed 0.4
	Player.DisplayName "VIP"
	Player.StartItem "TomGun"
	Player.StartItem "TomShell", 1
	Player.StartItem "Fist"
	Translation 2
	PainChance "TomDrain", 0
	LimitedToTeam 0
	+NOPAIN
	+NOSKIN
	-PICKUP

	States
	{
		Death:
			TNT1 A 0 ACS_ExecuteAlways(813, 0, 1)
			Goto Super::Death
		XDeath:
			TNT1 A 0 ACS_ExecuteAlways(813, 0, 1)
			Goto Super::XDeath
	}

}


// Trail spawner item - given by the looping ACS script
actor TomTrailSpawner : CustomInventory {

	States
	{
		Pickup:
			TNT1 A 0 A_SpawnItemEx("TomTrail", 0, 0, 5, 0, 0, 0, 0, SXF_NOCHECKPOSITION)
			Stop
	}

}

actor TomTrail {

	RenderStyle "Add"
	Translation "Ice"
	Alpha 1.0
	XScale 0.5
	YScale 0.5
	+NOINTERACTION
	+RANDOMIZE
	+CLIENTSIDEONLY

	States
	{
	Spawn:
		APLS AB 2 Bright A_FadeOut(0.01)
		Loop
	}

}


// Tom's weapon - instagib shotgun with a bit of a kick
actor TomGun : Shotgun {

	Inventory.PickupMessage "Personal Protection Weapon"
	Weapon.AmmoGive 1
	Weapon.AmmoUse 1
	Weapon.AmmoType "TomShell"
	+WEAPON.BFG
	+EXTREMEDEATH

	States
	{
		Fire:
			SHTG A 3
			SHTG A 0 A_FireBullets (5.6, 0, 7, 3000, "BulletPuff")
			SHTG A 0 A_PlaySound ("weapons/sshotf", CHAN_WEAPON)
			SHTG A 0 A_Recoil(20)
			SHTG A 7 A_GunFlash
			SHTG BC 5
			SHTG D 4
			SHTG CB 5
			SHTG A 3
			SHTG A 7 A_ReFire
			Goto Ready
	}

}

// Ammo for Tom's weapon - can only carry 1
actor TomShell : Ammo {

	Inventory.PickupMessage "Fry's Mint Cream"
	Inventory.Amount 1
	Inventory.MaxAmount 1
	Ammo.BackpackAmount 1
	Ammo.BackpackMaxAmount 1
	Inventory.Icon "SHELA0"

	States
	{
		Spawn:
			SHEL A -1
			Stop
	}

}

// custom Tom health pickup
// is actually a stationary 'monster' that only targets Tom
// and melees him when he gets close to fake being 'picked up'
actor TomHealth {

	MeleeRange 20
	XScale 0.6
	YScale 0.6
	+RANDOMIZE
	-COUNTITEM

	States
	{
		Spawn:
			TNT1 A 0 Thing_Hate(0, 999, 6)
			TOMH A 2 Bright A_Look
			TOMH A 6
			Loop
		See:
			TNT1 A 0 Thing_Hate(0, 999, 6)
			TOMH A 2 Bright A_Chase("FakePickup", 0, CHF_DONTMOVE)
			TOMH A 6 A_Chase("FakePickup", 0, CHF_DONTMOVE)
			Loop
		FakePickup:
			TNT1 A 0 A_GiveToTarget("Stimpack",1)
			TNT1 A 0 A_PlaySound("misc/i_pkup", CHAN_ITEM)
			Stop

	}

}

// custom pickup spot that spawns health for Tom
