import os
import zipfile

def compile_acs(src, dst):
	# TODO: run scripts through ACC and store them where they should be
	pass

def zip(src, dst):
    zf = zipfile.ZipFile("%s.pk3" % (dst), "w")
    abs_src = os.path.abspath(src)
    for dirname, subdirs, files in os.walk(src):
        for filename in files:
            absname = os.path.abspath(os.path.join(dirname, filename))
            arcname = absname[len(abs_src) + 1:]
            print 'zipping %s as %s' % (os.path.join(dirname, filename),
                                        arcname)
            zf.write(absname, arcname)
    zf.close()

zip("wadsrc", "KillVIP")